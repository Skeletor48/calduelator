const Calculator = require('./calculator');

const workHours = [9, 10, 11, 12, 13, 14, 15, 16];
const workDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

const calculator = new Calculator(workHours, workDays);

const testInput = [
  ['2:00 PM Friday', 2], // Should return 4:00PM Friday this week
  ['3:15 PM Friday', 2], //Since it would return 5:15PM Friday this week, we had to handle it
  // during the expiration Data handling so ideally it should return 9:15AM Monday in 1 week
  ['10:00 AM Wednesday', 2], //Based on the rules of meridien notation, it should return 12:00PM Wednesday this week
  ['8:00 AM Wednesday', 2], //Should warn user this date is out of office time and display the cor rect schedule
  ['2:00 PM Wednesday', 16], // Should return 2:00PM Friday this week
  ['2:00 PM Wednesday', 56], // Should return 2:00PM Friday in 1 week
  ['11:00 AM Wednesday', 5], // Should return 04:00PM Friday this week
  ['3:59 PM Wednesday',16], // Should return 03:59PM Friday this week
  ['4:00 PM Wednesday',1], // Should return 09:00PM Thursday this week
]

testInput.forEach(input => console.log('\n', calculator.calculateDueDate(input[0], input[1]), '\n'));
