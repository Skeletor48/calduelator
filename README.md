# CalDueLator

Due Date Calculator method implementation without third-party date manipulator libraries.


During the solution I intentionally not used the JS Date object and its very clever methods.
Hence the format of the input was not specified I used the '2:00 PM Friday' format.


In the Calculator class it seems like overengineering to parse the 12hour standard format to 24hour 'military' format, but as the test file shows it will come in handy if we want to expand the working hours to more than 12 hours or we want to use shifts. 

I choose to use the calculator as a class so this way we can use any other work hour/ work day arrays.  

A few test cases and what is the expected output:

['2:00 PM Friday',2], ->  Should return 4:00PM Friday this week

['3:15 PM Friday',2], -> Since it would return 5:15PM Friday this week, we had to handle it
                        during the expiration Data handling so ideally it should return 9:15AM Monday in 1 week

['10:00 AM Wednesday',2], -> Based on the rules of meridiem notation, it should return 12:00PM Wednesday

['8:00 AM Wednesday',2], -> Should warn user this date is out of office time and display the cor rect schedule

['2:00 PM Wednesday',16], -> Should return 2:00PM Friday this week

['2:00 PM Wednesday',56], -> Should return 2:00PM Friday in 1 week

['11:00 AM Wednesday',5], -> Should return 04:00PM Friday this week

['3:59 PM Wednesday',16], -> Should return 03:59PM Friday this week
  
['4:00 PM Wednesday',1], -> Should return 09:00PM Thursday this week


If I would have more time, I would implement another module, that can parse any type of date into the needed one.


UPDATE: I've just realized that I completly messed up the logic beacuse I can not count to 8.... :D 
I've used 7 hour long workdays. The funny part is, that after the fix the code is even easier. 
I was able to get rid of the `isWholeHour` property and could erase a messy logic wich was in account to handle the last hour submissions. 
