const Calculator = require('../calculator');
const mocha = require('mocha');
const chai = require('chai');
const expect = chai.expect;

const workHours = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
const workDays = ['Kalacs', 'Sajt', 'MakosTeszta', 'FluxusKondenztor', 'Peklapat', 'Fapapucs', 'Teglatest'];

const calculator = new Calculator(workHours, workDays);
describe('Test calculator', () => {
  let baseWorkHourIndex = 5 //eg. 2PM
  let baseWorkDayIndex = 2 //eg. MakosTeszta

  describe('Test _getExpirationData method', () => {
    it('should return the index of the due time hour and day from the work arrays if it is on the same day as the submission', () => {
      const turnaroundTime = 2
      const result = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime)
      expect(result.dueHour).to.equal(16);
      expect(result.dueDay).to.equal('MakosTeszta');
    })

    it('should return the index of the due time hour and day from the work arrays if it is a day after the submission', () => {
      const turnaroundTime = 5
      const result = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime)
      expect(result.dueHour).to.equal(19);
      expect(result.dueDay).to.equal('MakosTeszta');
    })

    it('should return the index of the due time hour and day from the work arrays even if it is multiple days later than the submission', () => {
      const turnaroundTime = 69

      const result = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime)
      expect(result.dueHour).to.equal(23);
      expect(result.dueDay).to.equal('Teglatest');
    })

    it('should return the count of weeks till the due date', () => {
      const turnaroundTime1 = 280
      const turnaroundTime2 = 1640
      const turnaroundTime3 = 1600
      const result1 = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime1)
      const result2 = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime2)
      const result3 = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime3)

      expect(result1.weekCounter).to.equal(3);
      expect(result2.weekCounter).to.equal(15);
      expect(result3.weekCounter).to.equal(15);
    })
    /////EDGE CASE WHEN THE DUE DATE IS A FEW SECONDS AFTER 4PM////
    it('should return the correct hour day week data if the workours are overloads because of the spare seconds', () => {
      const turnaroundTime = 10
      baseWorkDayIndex = 6

      const result = calculator._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime)
      expect(result.dueHour).to.equal(9);
      expect(result.dueDay).to.equal('Kalacs');
      expect(result.weekCounter).to.equal(1);
    })
  })

  describe('Test _parseMilitaryHours method', () => {
    it('should return 12 if standardHours is 12 and it is PM', () => {
      const standardHours = '12';
      const meridiemNotation = 'PM';

      const result = calculator._parseMilitaryHours(standardHours, meridiemNotation)
      expect(result).to.equal(12);
    })

    it('should return 24hour format if standardHours is not 12 and it is PM', () => {
      let standardHours;
      const meridiemNotation = 'PM';

      for (let i = 1; i < 12; i++) {
        standardHours = i.toString();
        const result = calculator._parseMilitaryHours(standardHours, meridiemNotation)
        expect(result).to.equal(i + 12);
      }
    })

    it('should return standardHours as militaryHours if standardHours is not 12 and it is not PM', () => {
      let standardHours;
      const meridiemNotation = 'AM';

      for (let i = 1; i < 12; i++) {
        standardHours = i.toString();
        const result = calculator._parseMilitaryHours(standardHours, meridiemNotation)
        expect(result).to.equal(i);
      }

    })
  })

  describe('Test _parseStandardHours method', () => {
    it('should return 12 if dueHourInMilitary is 0', () => {
      const dueHourInMilitary = 0;

      const result = calculator._parseStandardHours(dueHourInMilitary)
      expect(result).to.equal(12);
    })

    it('should return standard format if dueHourInMilitary is higher than 12', () => {
      let dueHourInMilitary;

      for (let i = 13; i < 23; i++) {
        dueHourInMilitary = i;
        const result = calculator._parseStandardHours(dueHourInMilitary)
        expect(result).to.equal(i - 12);
      }
    })

    it('should return standard format if standardHours is lower or equal to 12', () => {
      let dueHourInMilitary;

      for (let i = 1; i <= 12; i++) {
        dueHourInMilitary = i;
        const result = calculator._parseStandardHours(dueHourInMilitary)
        expect(result).to.equal(i);
      }
    })
  })

  describe('Test _recalculateMeridiemNotation method', () => {
    it('should return PM if dueHourInMilitary is higher than 11', () => {
      let dueHourInMilitary;

      for (let i = 12; i < 23; i++) {
        dueHourInMilitary = i;
        const result = calculator._recalculateMeridiemNotation(dueHourInMilitary)
        expect(result).to.equal('PM');
      }
    })

    it('should return AM if dueHourInMilitary is lower or equal to 11', () => {
      let dueHourInMilitary;

      for (let i = 0; i <= 11; i++) {
        dueHourInMilitary = i;
        const result = calculator._recalculateMeridiemNotation(dueHourInMilitary)
        expect(result).to.equal('AM');
      }
    })
  })
})
