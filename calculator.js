class Calculator {

  constructor(workHours, workDays) {
    this.workHours = workHours;
    this.workDays = workDays;
  }

  calculateDueDate(submitDateTime, turnaroundTime) {
    const [baseDay, meridiemNotation, baseHours, baseSeconds] = Object.values(this._getSubmitData(submitDateTime))
    const militaryHours = this._parseMilitaryHours(baseHours, meridiemNotation);

    // condition modified after the 24 hour barrier!!!!
    if (!this.workDays.includes(baseDay) || !this.workHours.includes(militaryHours)) {
      return this._getErrorMessage();
    }

    const baseWorkHourIndex = this._getBaseIndex(this.workHours, militaryHours);
    const baseWorkDayIndex = this._getBaseIndex(this.workDays, baseDay);
    const expirationData = this._getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime);
    const dueDateTime = this._formatExpirationData(expirationData, baseSeconds);

    return dueDateTime;
  };

  _getSubmitData(submitString) {
    const submitItems = submitString.split(' ');
    const timeString = submitItems[0];
    const timeUnits = timeString.split(':');

    return {
      baseDay: submitItems[2],
      meridiemNotation: submitItems[1],
      baseHours: timeUnits[0],
      baseSeconds: timeUnits[1],
      // object modified after the 24 hour barrier!!!!
    };
  };

  _parseMilitaryHours(baseHours, meridiemNotation) {
    if (meridiemNotation === 'PM') {
      return baseHours === '12' ? 12 : Number(baseHours) + 12;
    }
    return baseHours === '12' ? 0 : Number(baseHours);
  };

  _getBaseIndex(workArray, dateTimeUnit) {
    return workArray.indexOf(dateTimeUnit);
  };

  _getExpirationData(baseWorkHourIndex, baseWorkDayIndex, turnaroundTime) {
    const hourModifier = turnaroundTime % this.workHours.length;
    const dayModifier = (Math.floor(turnaroundTime / this.workHours.length) + (baseWorkHourIndex + hourModifier > this.workHours.length - 1)) % this.workDays.length;
    let weekCounter = Math.floor(turnaroundTime / this.workHours.length / this.workDays.length) + (baseWorkDayIndex + dayModifier > this.workDays.length - 1);
    let dueHoursIndex = this._getDueIndex(hourModifier, baseWorkHourIndex, this.workHours);
    let dueDaysIndex = this._getDueIndex(dayModifier, baseWorkDayIndex, this.workDays);

    // method modified after the 24 hour barrier!!!!

    return {
      dueHour: this.workHours[dueHoursIndex],
      dueDay: this.workDays[dueDaysIndex],
      weekCounter: weekCounter,
    }
  };

  _getDueIndex(modifier, baseUnitIndex, workArray) {
    if (baseUnitIndex + modifier > workArray.length - 1) {
      return baseUnitIndex = modifier - (workArray.length - baseUnitIndex);
    } else {
      return baseUnitIndex = baseUnitIndex + modifier;
    }
  };

  _formatExpirationData(expirationData, baseSeconds) {
    const [dueHour, dueDay, weekCounter] = Object.values(expirationData);

    const recalculatedMeridiemNotation = this._recalculateMeridiemNotation(dueHour);
    const dueHoursInStandard = this._parseStandardHours(dueHour);
    const dueTimeString = this._generateTimeString(dueHoursInStandard, baseSeconds);
    const weekCounterString = weekCounter === 0 ? `this week` : `in ${weekCounter} weeks`

    return (dueTimeString + recalculatedMeridiemNotation + ` ${dueDay} ` + weekCounterString);
  };

  _parseStandardHours(dueHourInMilitary) {
    if (dueHourInMilitary === 0 || dueHourInMilitary === 12) return 12;
    return dueHourInMilitary > 12 ? dueHourInMilitary - 12 : dueHourInMilitary;
  }

  _generateTimeString(dueHoursInStandard, dueSeconds) {
    return dueHoursInStandard.toString().concat(':' + dueSeconds);
  };

  _recalculateMeridiemNotation(dueHourInMilitary) {
    return dueHourInMilitary <= 11 ? 'AM' : 'PM'
  };

  _getErrorMessage() {
    return `You are not allowed to subbbmit issues at this time.
    Please try from ${this.workHours[0]}:00 AM to ${this._parseStandardHours(this.workHours[this.workHours.length - 1])}:00 PM 
    on ${this.workDays[0]} to ${this.workDays[this.workDays.length - 1]}!`
  }
}

module.exports = Calculator;
